const express = require('express');
const OrderApi = express.Router();

const OrderModel = require('../models/orderModel');

//Middleware
OrderApi.use((req, res, next) => {
	console.log("Middleware");
	next();
});

//Read all orders  http://localhost:8000/api/posts?page=1&per_page=5
OrderApi.get('/', (req, res) => {
    const { page=1, per_page=5 } = req.query;
    OrderModel.find({})
    .skip((page-1)*per_page)
    .limit(per_page*1)
    .then((orders) => {
        res.send({ data: orders });
    })
    .catch((error) => {
        res.send({ error });
    });
});

// Read order by status
OrderApi.get('/:status', (req, res) => {
    const { status } = req.params;
    OrderModel.find(status)
    .then((ordersFound) => {
        res.send({ data: ordersFound });
    })
    .catch((error) => {
        res.send({ error });
    })
})

// Change status or save note of order
OrderApi.put('/:orderId', (req, res) => {
    const { orderId } = req.params;
    const { status, note } = req.body;
    OrderModel.findById(orderId)
    .then((orderFound) => {
        if(!orderFound) res.send({ error: "Order not exist!!!" });
        else {
            if(status) orderFound.status = status;
            if(note) orderFound.note = note;
            return orderFound.save();
        }
    })
    .then((orderUpdate) => {
        res.send({ data: orderUpdate });
    })
    .catch((error) => {
        res.send({ error });
    });
})

module.exports = OrderApi;