const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const OrderApi = require('./routers/orderApi');

const app = express();

app.use("/public", express.static("public"));
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/order',
    { useNewUrlParser: true },
    (err) => {
    if(err) console.log(err)
    else console.log('DB success')
})

app.use('/api/orders', OrderApi);

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/view/index.html')
})

app.listen(8000, (err) => {
    if(err) console.log(err);
    else console.log("Server: start success!");
});