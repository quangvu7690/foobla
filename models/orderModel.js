const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    orderId: { type: Number, required: true },
    items: { type: String, required: true },
    custumerInfo: { type: String, required: true },
    shippingAddress: { type: String, required: true },
    time: { type: Date, default: new Date() },
    total: { type: Number, required: true, default: 1 },
    status: { 
        type: String, 
        required: true,
        enum: [ 'pending', 'processing', 'shipped' ],
    },
    note: { type: String, default: '' }
});

module.exports = mongoose.model("order", OrderSchema);
